mod configuration;

use std::process::Command;

fn main() {
    Command::new("fish").spawn().unwrap().wait().unwrap();
}
