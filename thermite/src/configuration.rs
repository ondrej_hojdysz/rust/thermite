use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct CommandConfig {
    name: String,
    args: Vec<String>,
}

impl CommandConfig {
    pub fn prepare(&self) -> std::process::Command {
        let mut command = std::process::Command::new(&self.name);
        for arg in &self.args {
            command.arg(arg);
        }

        command
    }
}

#[derive(Deserialize, Serialize)]
pub struct Configuration {
    shell: CommandConfig,
}
